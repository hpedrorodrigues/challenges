class Challenge(object):
    def __init__(self, max_number):
        self.max_number = max_number

    def apply_conjecture_rules(self, x):
        if x <= 1:
            return x
        if x % 2 == 0:
            return x / 2
        else:
            return 3 * x + 1

    def count_sequence_items(self, x):
        count = 0
        current = x

        while current > 1:
            current = self.apply_conjecture_rules(current)
            count += 1

        count += 1

        return count

    def find_number_with_largest_sequence(self):
        largest_sequence_size = 0
        number = 0

        for i in range(1, self.max_number):
            sequence_size = self.count_sequence_items(i)

            if sequence_size > largest_sequence_size:
                largest_sequence_size = sequence_size
                number = i

        return number

    def print_result(self):
        print(self.find_number_with_largest_sequence())


Challenge(999999).print_result()
