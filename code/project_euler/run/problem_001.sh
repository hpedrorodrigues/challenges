#!/usr/bin/env bash

set -e

echo
echo "Problem 001"
echo
echo "If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23."
echo "Find the sum of all the multiples of 3 or 5 below 1000."
echo
echo "-"
echo "Clojure"
./solutions/clojure/problem_001.clj
echo "-----"
echo "Elixir"
./solutions/elixir/problem_001.exs
echo "-----"
echo "Groovy"
./solutions/groovy/problem_001.groovy
echo "-----"
echo "Haskell"
./solutions/haskell/problem_001.hs
echo "-----"
echo "Java"
./scripts/run_java_script.sh solutions/java/problem_001.java
echo "-----"
echo "Javascript"
./solutions/javascript/problem_001.js
echo "-----"
echo "PHP"
./solutions/php/problem_001.php
echo "-----"
echo "Python"
./solutions/python/problem_001.py
echo "-----"
echo "Ruby"
./solutions/ruby/problem_001.rb
echo "-----"
echo "Scala"
./solutions/scala/problem_001.scala
echo "-----"
echo "Shell"
./solutions/shell/problem_001.sh
echo