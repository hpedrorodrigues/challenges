# Project Euler

**Overview**

Project Euler is a collection of interesting computational problems intended to be solved with 
computer programs. Most of the problems challenge your skills in algorithm design rather than your 
knowledge of mathematics.
Thanks to short concrete problems and number-only solutions you don’t have to thinker with IDEs, 
GUI designers, programming libraries and frameworks, so Project Euler is an excellent tool for 
learning a new programming language or improving your core programming skills.

See [Project Euler](https://projecteuler.net/) for more details.