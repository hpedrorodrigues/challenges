#!/usr/bin/env scala

/*
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
*/

def primeFactors(n: Long): List[Long] =
  (2 to math.sqrt(n).toInt).find(n % _ == 0).fold(List(n))(i => i.toLong :: primeFactors(n / i))

println(primeFactors(600851475143L).last)