#!/usr/bin/php5
<?php

/*
  If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
  The sum of these multiples is 23.

  Find the sum of all the multiples of 3 or 5 below 1000.
*/

echo array_sum(array_filter(range(1, 999), function($i) { return ($i % 3 == 0 || $i % 5 == 0); })).PHP_EOL;

?>