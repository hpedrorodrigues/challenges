#!/usr/bin/env node

'use strict';

/*
 Each new term in the Fibonacci sequence is generated by adding the previous two terms.
 By starting with 1 and 2, the first 10 terms will be:

 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

 By considering the terms in the Fibonacci sequence whose values do not exceed four million, find
 the sum of the even-valued terms.
 */

var LIMIT = 4000000
    , previous = 0
    , current = 1
    , next
    , counter = 0
    , sum = 0;

function isEven(number) {
 return number % 2 == 0;
}

while (current < LIMIT) {
 if (isEven(current)) {
  sum += current;
 }
 next = previous + current;
 previous = current;
 current = next;
 counter++;
}

console.log(sum);