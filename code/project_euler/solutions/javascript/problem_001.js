#!/usr/bin/env node

'use strict';

/*
 If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
 The sum of these multiples is 23.

 Find the sum of all the multiples of 3 or 5 below 1000.
 */

function range(end) {
    var array = new Array(end);
    for (var i = 0; i < end; i++) {
        array[i] = i;
    }
    return array;
}

var sum = range(1000)
    .filter(function (elem) { return elem % 3 == 0 || elem % 5 == 0; })
    .reduce(function (a, b) { return a + b; });

console.log(sum);