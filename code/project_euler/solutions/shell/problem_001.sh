#!/usr/bin/env bash

#  If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
#  The sum of these multiples is 23.
#
#  Find the sum of all the multiples of 3 or 5 below 1000.

result=0

for n in $(seq 1 999)
do
  if (( $n % 5 == 0 || $n % 3 == 0 )); then
     ((result = ($result + $n)))
  fi
done

echo $result