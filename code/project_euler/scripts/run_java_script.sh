#!/bin/bash

set -e

TEMP_DIRECTORY=/tmp
TEMPFILE=`mktemp $TEMP_DIRECTORY/ScriptXXXXX|sed -e 's/\.//g'`
CLASS_NAME=`basename "$TEMPFILE"`
JAVA_SOURCE=$TEMPFILE.java

cat $1 | xargs -0 >> $JAVA_SOURCE.tmp

## change the name of the class to match the file
sed "s/public class Script /public class $CLASS_NAME /g" $JAVA_SOURCE.tmp >$JAVA_SOURCE

## compile the java
javac $JAVA_SOURCE 

## run the class using all passed in parameters from the command line
java -classpath $TEMP_DIRECTORY $CLASS_NAME