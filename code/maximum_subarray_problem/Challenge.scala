class Challenge {

  def findSetWithLargestSum(sequence: List[Int]) = {
    var maxSum = 0
    var sequenceStart = 0
    var sequenceEnd = 0

    sequence.indices foreach { start =>

      sequence.slice(start, sequence.size).indices foreach { end =>

        (start to end).map(sequence).sum match {
          case sum if sum > maxSum =>
            maxSum = sum
            sequenceStart = start
            sequenceEnd = end
          case _ =>
        }
      }
    }

    (sequenceStart, sequenceEnd)
  }
}

var (start, end) = new Challenge().findSetWithLargestSum(List(2, -4, 6, 8, -10, 100, -6, 5))

println(s"Início = $start - Fim = $end")